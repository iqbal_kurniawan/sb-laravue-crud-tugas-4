<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.completed{
			text-decoration: line-through;
		}
	</style>
</head>
<body>
	<div id="app">
		<h3>Todo List</h3>
		<input type="text" v-model="newTodo" @keyup.enter="addTodo">
		<ul>
			<li v-for="(todo, index) in todos">
				<span v-bind:class="{'completed' : todo.done}">@{{todo.text}}</span>
				<button type="button" v-on:click="removeTodo(index, todo)">X</button>
				<button type="button" v-on:click="toggleComplete(todo)">DONE</button>
			</li>
		</ul>
	</div>
	
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

	<script type="text/javascript">
		new Vue({
			el: "#app",
			data: {
				newTodo: "",
				todos:[]
			},
			methods:{
				addTodo: function(){
					let textInput = this.newTodo.trim();
					if(textInput){
						 // POST /someUrl
						this.$http.post('/api/todo', {text: textInput}).then(response => {
							this.todos.unshift(
								{text: textInput, done: 0}
							)
							this.newTodo = ''
						});
					}
					console.log(textInput);
				},
				removeTodo: function(index, todo ){
					console.log(todo);
					// POST /someUrl
					this.$http.post('/api/todo/delete/' +todo.id).then(response => {
						this.todos.splice(index,1)	
					});
				},
				toggleComplete: function(todo){
					
					this.$http.post('/api/todo/change-done-status/' +todo.id).then(response => {
						todo.done = !todo.done
					});
				},
			},
			mounted:function(){
					  // GET /someUrl
					  this.$http.get('/api/todo').then(response => {
					  	let result = response.body.data;
					  	this.todos = response.body.data;
					  	console.log(response.body.data)

					  });
			}
			
		});
	</script>
</body>
</html>